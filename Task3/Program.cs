﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();
            Console.WriteLine("Введите имя автора:");
            book.Author = Console.ReadLine();

            Console.WriteLine("Введите описание книги:");
            book.Title = Console.ReadLine();

            Console.WriteLine("Введите текст книги:");
            book.Content = Console.ReadLine();

            book.Show();

            Console.ReadLine();

        }
    }
}
