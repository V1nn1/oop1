﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Title
    {
        string text;

        public string Text
        {
            get
            {
                if (text != null)
                    return text;

                else
                    return "Нет описания";
            }
            set
            {
                if (value!="")
                text = value;
            }
        }
        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(Text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
