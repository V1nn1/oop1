﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Content
    {
        string text;

        public string Text
        {
            get
            {
                if (text != null)
                    return text;

                else
                    return "Нет текста";
            }
            set
            {
                if (value != "")
                    text = value;
            }
        }
        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(Text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
