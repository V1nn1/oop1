﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Book
    {
        private Author _author ;
        private Title _title;
        private Content _content;

        public Book()
        {
            _author = new Author();
            _title = new Title(); 
            _content = new Content();
        }

        public void Show()
        {
            _title.Show();
            _author.Show();
            _content.Show();
            
        }

        public string Author
        {
            set { _author.Text = value; }
        }
        public string Title
        {
            set { _title.Text = value; }
        }

        public string Content
        {
            set { _content.Text = value; }
        }

    }
}
