﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Rectangle
    {
        private double side1;
        private double side2;
        private double area;
        private double perimeter;

        public double Side1 { get { return side1; } }
        public double Side2 { get { return side2; } }
        public double Area { get { return area; } }
        public double Perimeter { get { return perimeter; } }

        public Rectangle (double side1, double side2)
        {
            this.side1 = side1;
            this.side2 = side2;
            area = AreaCalculator();
            perimeter = PerimeterCalculator();
        }

        public double AreaCalculator()
        {
            return Side1* Side2;
             
        }

        
        public double PerimeterCalculator()
        {
            return (Side1 + Side2) * 2;
        }

    }
}
