﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите сторону a:");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите сторону b:");
            double b = Convert.ToDouble(Console.ReadLine());
            Rectangle rectangle = new Rectangle(a, b);

            Console.WriteLine(rectangle.Area);
            Console.WriteLine(rectangle.Perimeter);
            Console.ReadLine();

        }
    }
}
