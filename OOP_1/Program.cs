﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Address address = new Address();
            address.Index = "04123";
            address.Country = "Украина";
            address.City = "Киев";
            address.Street = "Крещатик";
            address.House = "1";
            address.Apartment = "1";

            Console.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}",address.Index,address.Country,address.City,address.Street,address.House,address.Apartment);
            Console.ReadKey();
        }
    }
}
